CPP=clang++
CPPFLAGS=$(DEBUG) -Wall -fPIC -std=c++17 


all: devpack

devpack: main.cpp
	$(CPP) $(CPPFLAGS) -v main.cpp -o devpack
